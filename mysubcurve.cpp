#include "mysubcurve.h"

//namespace GMlib {

MySubCurve::MySubCurve(PCurve<float,3>* g, float s, float e, float t)
{
    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

    set(g, s, e, t);

    GMlib::DVector<GMlib::Vector<float,3> > tr = g->evaluateParent(t, 0);
    _trans = tr[0];
    this->translate( _trans );
}


MySubCurve::~MySubCurve() {}


void MySubCurve::eval( float t, int d, bool /*l*/ )
{
    this->_p     = _g->evaluateParent(t , d);
    this->_p[0] -= _trans;
}

float MySubCurve::getStartP()
{
    return _s;
}


float MySubCurve::getEndP()
{
    return _e;
}


bool MySubCurve::isClosed() const
{
    return false;
}

void MySubCurve::set(PCurve<float,3>* g, float s, float e, float t)
{
    _g = g;
    _s = s;
    _e = e;
    _t = t;
}

void MySubCurve::localSimulate(double dt) {

    this->rotate(dt, GMlib::Vector<float, 3>(2.0, 2.0, 1.0));
    this->replot(1000, 1);

}


//} // END namespace GMlib

