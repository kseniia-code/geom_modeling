#ifndef MYBEZIER_H
#define MYBEZIER_H

#include <parametrics/gmpcurve>
#include <parametrics/gmpcircle>
#include "mysubcurve.h"
#include "myshape.h"

class MyBezier : public GMlib::PCurve<float,3> {
    GM_SCENEOBJECT(MyBezier)
    public:
    MyBezier( PCurve<float,3>* g, float s, float e, float t, int d);
    ~MyBezier();

    //int                       getDegree() const;
    GMlib::DVector<float>       generateKnotVector(float start, float end);
    bool                        isClosed();

protected:
    float                                       _delta;
    GMlib::DVector<GMlib::Vector<float,3>>      _cp; //control points
    //GMlib::DVector<GMlib::Vector<float,3>>      _controlp;
    int                                         _d;
    PCurve<float,3>*                            _g;

    float                                       _s;
    float                                       _e;
    void      makeControlPoints(float t, int d);

    GMlib::Vector<float,3>                      _trans;

    void                  eval(float t, int d, bool = true);
    float                 getEndP();
    float                 getStartP();

    GMlib::DMatrix<double> BezierMatrix(int d, double t, double delta);

    void set(PCurve<float,3>* g, float s, float e, float t, int d);

}; // END class MyBezier


#endif // MYBEZIER_H


