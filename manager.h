#ifndef MANAGER_H
#define MANAGER_H
#include <windows.h>

class GMlibWrapper;
class MyShape;
//class MyBspline;

namespace GMlib {
    template <typename T>
    class PButterfly;
}
//class MyShape;
//#include "myshape.h"
#include "mybspline.h"
#include "mysubcurve.h"
#include "mybezier.h"


#include <scene/gmsceneobject>
#include <gmParametricsModule>

class Manager : public GMlib::SceneObject {
    GM_SCENEOBJECT(Manager)

	public:
		Manager();
		~Manager();

        void insertScene();


  protected:

        void localSimulate(double dt);

	private:


        GMlib::Camera* cam;
        GMlibWrapper* wrapper;
        GMlib::PButterfly<float>* butterfly;
        MyShape* my_shape;
        MyBspline* my_bspline;
        GMlib::DVector<float> _knotv;
        MySubCurve* my_subcurve;
        MyBezier* my_bezier;
        GMlib::PCircle<float>* circle;


};

#endif // MANAGER_H

