#include "mybezier.h"

void MyBezier::set(PCurve<float,3>* g, float s, float e, float t, int d)
{
    this->_g = g;
    this->_s = s;
    this->_e = e;
    this->_t = t;
    this->_d = d;
}

MyBezier::MyBezier(PCurve<float,3>* g, float s, float e, float t, int d) {

    this->_dm = GMlib::GM_DERIVATION_EXPLICIT;

    set(g, s, e, t, d);

    GMlib::DVector<GMlib::Vector<float,3> > tr = g->evaluateParent(t, d);
    _trans = tr[0];
    this->translateParent( _trans );

    //GMlib::DMatrix<double> A(BezierMatrix(3, 0.3, 1));
    //std::cout<<A.getDim1();
    //    std::cout<<A;
    //    std::cout.flush();

    //scale
    _delta = 1.0/(_e - _s);
    //_delta = 1.0;

    this->makeControlPoints(t, d);
    //        std::cout<<this->_cp;
    //        std::cout.flush();

}

MyBezier::~MyBezier() {}


void MyBezier::eval( float t, int d, bool) {

    this->_p.setDim(d+1);

    //_cp = makeControlPoints(t, d);
    GMlib::DMatrix<double> bhp;
    //bhp = this->BezierMatrix( d, (t - _s)/(_e - _s), _delta );
    bhp = this->BezierMatrix( d, t, _delta );

    //std::cout<<bhp.getDim1();
    //    std::cout<<bhp;
    //    std::cout.flush();

    this->_p = bhp * this->_cp;

    this->_p[0] -= _trans;

    //            std::cout<<this->_p;
    //            std::cout.flush();

    //this->_p.setDim(d+1);

}


float MyBezier::getStartP() {

    //return _s;
    return 0.0;
}

float MyBezier::getEndP() {

    //return _e;
    return 1.0;
}

bool MyBezier::isClosed(){

    return this->_g->isClosed();
    //return true;
}

//points p
void MyBezier::makeControlPoints(float t, int d){

    //_delta = 1.0/(_e - _s);

    GMlib::DMatrix<double> m = BezierMatrix(d, (t - _s)/(_e - _s), _delta).invert();
    //GMlib::DMatrix<double> m = BezierMatrix(d, t, _delta).invert();
    //GMlib::DVector<GMlib::Vector<float, 3>> v = this->_g->evaluateParent(t, d);
    GMlib::DVector<GMlib::Vector<float, 3>> v = _g->evaluateParent(t, d);
    _cp = m*v;

    //    for(int i = 0; i < _cp.getDim(); i++)
    //        _cp[i] -= v[0];
    //    this->translate(v[0]);

    std::cout<<this->_cp;
    std::cout.flush();

    //return _cp;

}

//matrix of basis functions
//Generalized Bernstein/Hermite matrix
GMlib::DMatrix<double> MyBezier::BezierMatrix(int d, double t, double delta){

    GMlib::DMatrix<double> B(d+1,d+1);
    B[d-1][0] = 1 - t;
    B[d-1][1] =  t;

    for(int i = d - 2; i >= 0; i--){

        B[i][0] = (1-t)*B[i+1][0];

        for(int j = 1; j < d-i; j++){

            B[i][j] = t*B[i+1][j-1]+(1-t)*B[i+1][j];

        }

        B[i][d-i] = t*B[i+1][d-i-1];

    }

    B[d][0] = -delta;
    B[d][1] = delta;

    for(int k = 2; k <= d; k++){

        double s = k*delta;

        for(int i = d; i > d-k; i--){

            B[i][k] = s*B[i][k-1];

            for(int j = k-1; j > 0; j--){

                B[i][j] = s*(B[i][j-1] - B[i][j]);

            }

            B[i][0] = (-s)*B[i][0];

        }

    }

    return B;

}



