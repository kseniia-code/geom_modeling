#ifndef MYPSUBCURVE_H
#define MYPSUBCURVE_H

#include <parametrics/gmpcurve>
#include "myshape.h"

//namespace GMlib {

  class MySubCurve : public GMlib::PCurve<float,3> {
    GM_SCENEOBJECT(MySubCurve)
  public:
    MySubCurve( PCurve<float,3>* g, float s, float e, float t);
    virtual ~MySubCurve();

    // virtual functions from PSurf
    virtual bool            isClosed() const;
    // Local functions
    //void                    togglePlot();
    void                      localSimulate(double dt);

  protected:

    PCurve<float,3>*                _g;
    float                   _s;
    float                   _t;
    float                   _e;
    GMlib::Vector<float,3>     _trans;

    // virtual functions from PSurf
    void                    eval( float t, int d, bool l = false );
    float                   getEndP();
    float                   getStartP();

  private:

    // Local help functions
    void set(PCurve<float,3>* g, float s, float e, float t);

  }; // END class MySubCurve

//} // END namepace GMlib


// Include PSubCurve class function implementations
//#include "gmpsubcurve.c"


#endif // MYPSUBCURVE_H

