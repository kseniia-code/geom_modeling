#include "mybspline.h"

//for subcurves
MyBspline::MyBspline(PCurve<float,3>* g, int n) {

    this->_n = n;
    this->_g = g;
    _scale = 1.0;
    this->_s = g->getParStart();
    this->_e = g->getParEnd();
    _bezier = false;
    generateKnotVector(_s, _e);
    createSubcurves(g);

}

//for bezier curves
MyBspline::MyBspline(PCurve<float,3>* g, int n, int d) {

    this->_n = n;
    this->_degree = d;
    this->_g = g;
    _scale = 1.0;
    _bezier = true;
    this->_s = g->getParStart();
    this->_e = g->getParEnd();
    generateKnotVector(_s, _e);

    //    std::cout<<_kv;
    //    std::cout.flush();

    createBeziercurves(g, d);

}



MyBspline::~MyBspline() {}

float MyBspline::getT(float t, int index){

    if (_bezier){
        return ((t - _kv[index-1])/(_kv[index+1] - _kv[index-1]));
    }
    else{
        return t;
    }


}

void MyBspline::createSubcurves(PCurve<float,3> *g){

    //our subcurve
    _loc.setDim( _n);

    //generateKnotVector(_s, _e);

    //summarize all subcurves
    for(int i = 1; i < _n; i++){
        _loc[i-1] = new MySubCurve(g, _kv[i-1], _kv[i+1], _kv[i]);
    }

    if (_g->isClosed())
        _loc[_n-1] = _loc[0];
    else
        _loc[_n-1] = new MySubCurve(g, _kv[_n-1], _kv[_n+1], _kv[_n]);


}

void MyBspline::createBeziercurves(PCurve<float,3>* g, int d){

    //our beziercurve
    _loc.setDim( _n);

    //generateKnotVector(_s, _e);

    //summarize all subcurves
    for(int i = 1; i < _n; i++){
        _loc[i-1] = new MyBezier(g, _kv[i-1], _kv[i+1], _kv[i], d);
        _loc[i-1]->toggleDefaultVisualizer();
        _loc[i-1]->replot(1000,2);
        _loc[i-1]->setColor(GMlib::GMcolor::Blue);
        insert(_loc[i-1]);
    }

      //if (g->isClosed()){
     //   _loc[_n-1] = _loc[0];
    //}
    //    else{
    _loc[_n-1] = new MyBezier(g, _kv[_n-1], _kv[_n+1], _kv[_n], d);

    _loc[_n-1]->toggleDefaultVisualizer();
    _loc[_n-1]->replot(1000,2);
    _loc[_n-1]->setColor(GMlib::GMcolor::Blue);
    insert(_loc[_n-1]);

   // }

}


GMlib::DVector<float> MyBspline::generateKnotVector(float start, float end) {

    int order = 2;

    _kv.setDim( _n + order);
    int step_knots = _kv.getDim() - ( order * 2 );
    float delta = (end - start)/(step_knots+1);

    int i = 0;

    // Set the start knots
    for( ; i < order; i++ )
        _kv[i] = start;

    // Set the "step"-knots
    for( int j = 0; j < step_knots; j++ )
        _kv[i++] = start+(j+1)*delta;

    // Set the end knots
    //knot_value++;
    for( ; i < ( _n + order); i++ )
        _kv[i] = end;

    return _kv;

}



void MyBspline::eval(float t, int d, bool){

    //number of derivatives to compute
    this->_p.setDim(d+1);

    //c(t) = sum ci(t)*Bi(t)

    int index = findIndex(t);
    //    std::cout<<index;
    float w = (t -_kv[index])/(_kv[index+1] - _kv[index]);
    //int _scale = 1.0;
    GMlib::DVector<float> BFunction = B(w, d, _scale);

    GMlib::DVector<GMlib::Vector<float,3>> c1 = _loc[index-1]->evaluateParent(getT(t, index), d);
    GMlib::DVector<GMlib::Vector<float,3>> c2 = _loc[index]->evaluateParent(getT(t, index+1), d);

    //this->_p[0] = BFunction[0]*c1[0] + (1 - BFunction[0]*c2[0]);
    //this->_p[1] = c2[0] + BFunction[0]*(c1[0][0] - c2[0]);

    this->_p[0] = c1[0]+ BFunction[0]*(c2[0] - c1[0]);
    if( d > 0)
        this->_p[1] = c1[1]+ BFunction[0]*( c2[1] - c1[1])+ BFunction[1]*( c2[0] -  c1[0]) ;
    if( d > 1)
        this->_p[2] = c1[2]+ BFunction[0]*( c2[2] -  c1[2])+ 2*BFunction[1]*( c2[1] -  c1[1])+ BFunction[2]*(c2[0] -  c1[0]) ;

}


float MyBspline::getStartP() {

    //need to find according the knot vector
    return _kv[1];


}

float MyBspline::getEndP() {

    //    float a;
    //    //need to find according the knot vector
    //    for( int i=0; i < ( _n + 2); i++ )
    //        a = _kv[i-2];
    //    return a;

    return _kv[_kv.getDim()-2];
}

bool MyBspline::isClosed(){

    //return _g->isClosed();
    return true;
}

GMlib::DVector<float> MyBspline::B(float t, float d, float scale) {

    GMlib::DVector<float> v;
    v.setDim(d+1);
    v[0] = 3*pow(t,2) - 2*pow(t,3);
    if (d>0)
        v[1] = (6*t - 6*pow(t,2))*scale;
    if (d>1)
        v[2] = (6 - 12*t)*scale;
    if (d>2)
        v[3] = -12*scale*scale;

    return v;

}

int MyBspline::findIndex( float t)
{
    //return (this->_no_sam-1)*(t-this->getParStart())/(this->getParDelta())+0.1;
    //return (20-1)*(t-this->getParStart())/(this->getParDelta())+0.1;

    for (int i = 1; i < _kv.getDim() - 2; i++){
        if (t>=_kv[i] && t<_kv[i+1])
            return i;
    }

    return  _kv.getDim() - 3;

}

void MyBspline::localSimulate(double dt) {

    this->rotate(dt, GMlib::Vector<float, 3>(0.0, 2.0, 1.0));
    //    this->replot(1000, 1);

}



