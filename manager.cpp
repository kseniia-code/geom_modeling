#include "manager.h"
#include "gmlibwrapper.h"
#include "utils.h"
#include "myshape.h"

#include <iostream>
#include <conio.h>
#include <windows.h>

//#include <parametrics/gmpsurfpointsvisualizer>

Manager::Manager()
{
    this->toggleVisible();
    insertScene();

}

Manager::~Manager()
{

}

void Manager::insertScene(){

    //sphere = new GMlib::PSphere (1.5);
    //sphere->toggleDefaultVisualizer();
    //plane_ground->getDefaultVisualizer()->setDisplayMode(GMlib::Visualizer::DISPLAY_MODE_WIREFRAME);
    //sphere->replot(30,10,1,1);
    //robot->insert(sphere);

    //example with butterfly
    //        butterfly = new GMlib::PButterfly<float>;
    //        butterfly->toggleDefaultVisualizer();
    //        butterfly->replot(1000,2);
    //        this->insert(butterfly);

    //our initial curve, which copy we will make

    //std::cout<<"message";

    //circle = new GMlib::PCircle<float>(5.0);
    //    circle->toggleDefaultVisualizer();
    //    circle->replot(1000,2);
    //    this->insert(circle);

    my_shape = new MyShape();
            my_shape->toggleDefaultVisualizer();
            my_shape->replot(1000,2);
            this->insert(my_shape);

    //std::cout<<"message";

    //we need to check if the curve closed or open
    //checked manually
    //GMlib::DVector<GMlib::Vector<float,3>> a1 = my_shape->evaluate(my_shape->getParStart(),1);
    //GMlib::DVector<GMlib::Vector<float,3>> a2 = my_shape->evaluate(my_shape->getParEnd(),1);
    //    std::cout<<a1<<std::endl;
    //    std::cout<<a2<<std::endl;

//    my_subcurve = new MySubCurve(my_shape, 0, 5, 1);
//            my_subcurve->toggleDefaultVisualizer();
//            my_subcurve->replot(1000,1);
//            this->insert(my_subcurve);

            //create bezier curve for our curve
//            my_bezier = new MyBezier(my_shape, 0.0, 1.0, 0.5, 2);
//            my_bezier->toggleDefaultVisualizer();
//            my_bezier->replot(1000,2);
//            my_bezier->setColor(GMlib::GMcolor::Blue);
//            my_bezier->translate(GMlib::Vector<float,3>(0.0,0.1,0.1));
//            this->insert(my_bezier);

    //create bezier curve for circle (test)
//        my_bezier = new MyBezier(circle, 0.0, 1.63, 0.8, 2);
//        my_bezier->toggleDefaultVisualizer();
//        GMlib::PCurveDerivativesVisualizer<float, 3>* myvis = new GMlib::PCurveDerivativesVisualizer<float, 3>();
//        my_bezier->insertVisualizer(myvis);
//        my_bezier->replot(10,2);
//        my_bezier->setColor(GMlib::GMcolor::Blue);
//        my_bezier->translate(GMlib::Vector<float,3>(0.0,0.1,0.1));
//        this->insert(my_bezier);


    my_bspline = new MyBspline(my_shape, 5);
    my_bspline->toggleDefaultVisualizer();
    my_bspline->replot(1000,2);
    my_bspline->setColor(GMlib::GMcolor::Aqua);
    this->insert(my_bspline);

    //generate a knot vector
    //_knotv = my_bspline->generateKnotVector(0, 5);

    //std::cout<<_knotv;
    //std::cout.flush();

    this->setSurroundingSphere(GMlib::Sphere<float, 3>(100));

}



void Manager::localSimulate(double dt) {

    //rotate(GMlib::Angle(180)*dt, GMlib::Vector<float, 3>(0.0, 0.0, 1.0));
    //replot(1000, 1);

}






