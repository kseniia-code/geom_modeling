#ifndef MYSHAPE_H
#define MYSHAPE_H

//#include "myshape.h"

#include <parametrics/gmpcurve>
//namespace GMlib {

  //template <typename T>
  class MyShape : public GMlib::PCurve<float,3> {
    GM_SCENEOBJECT(MyShape)
  public:
    MyShape();
    ~MyShape();

    bool          isClosed() const;
    float             getEndP();
    float             getStartP();
    void      localSimulate(double dt);


protected:
    float             _size;
    void	          eval(float t, int d, bool l = true);


  }; // END class MyShape

//} // END namepace GMlib

// Include PButterfly class function implementations
//#include "gmpbutterfly.c"

#endif // MYSHAPE_H




