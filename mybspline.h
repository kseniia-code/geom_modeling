#ifndef MYBSPLINE_H
#define MYBSPLINE_H

#include <parametrics/gmpcurve>
#include "mysubcurve.h"
#include "myshape.h"
#include "mybezier.h"

  class MyBspline : public GMlib::PCurve<float,3> {
    GM_SCENEOBJECT(MyBspline)
  public:
    MyBspline(PCurve<float,3> *g, int n);
    MyBspline(PCurve<float, 3> *g, int n, int d );
    virtual ~MyBspline();

    //int                       getDegree() const;
    GMlib::DVector<float>       generateKnotVector(float start, float end);
    void                        createSubcurves(PCurve<float,3>* g);
    void                        createBeziercurves(PCurve<float,3> *g, int d);
    int                         findIndex( float t);
    bool                        isClosed();
    void                        localSimulate(double dt);



  protected:
    float                                       _scale;
    GMlib::DVector<float>                       _kv;
    int                                         _degree;
    int                                          _n;
    PCurve<float,3>*                            _g;
//    GMlib::DVector<MySubCurve*>                 _subc;
//    GMlib::DVector<MyBezier*>                   _bezierc;
    GMlib::DVector<PCurve*>                     _loc;
    float                   _s;
    float                   _e;
    bool _bezier;
    float getT(float t, int index);




    void                  eval(float t, int d, bool = true);
    float                 getEndP();
    float                 getStartP();
    GMlib::DVector<float> B(float t, float d, float scale);


  }; // END class MyBspline


#endif // MYBSPLINE_H


